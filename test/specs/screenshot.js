function screenshotName(filename) {
    const browserName = browser.desiredCapabilities.browserName;
    const browserVersion = browser.desiredCapabilities.version;
    const re = new RegExp("(.+)\\.(png)", "g");
    return filename.replace(re, `$1-${browserName}-${browserVersion}.$2`);
}


describe('screenshot',  () => {
    it('captures a full-screen image',  function() {
         browser.url('/');
         browser.saveDocumentScreenshot(screenshotName('screenshots/full-page.png'));
    });
});
