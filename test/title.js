var assert = require('assert');
describe('titleCheck', () => {
    it('has the expected page title', () => {
        browser.url('/');
        assert.equal(browser.getTitle(), 'Montefiore PrEP');
    });
});
