gemini.suite('homepage', (suite)=>{
    suite.setUrl('/')
        .setCaptureElements('.hero')
        .capture('plain', function(actions){
            actions.waitForElementToShow('.hero')
        })
})