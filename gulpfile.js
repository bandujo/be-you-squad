var gulp = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-sass'),
	notify = require('gulp-notify'),
	util = require('gulp-util'),
	plumber = require('gulp-plumber'),
	prefix = require('gulp-autoprefixer'),
    copy = require('gulp-copy'),
    del = require('del'),
    twig = require('gulp-twig'),
    ftp = require('vinyl-ftp'),
    exit = require('gulp-exit');

var connect = require('gulp-connect');
var webdriver = require( 'gulp-webdriver');
var scsslint = require('gulp-scss-lint');
var replace = require('gulp-string-replace');
const babel = require('gulp-babel');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
const imageResize = require('gulp-image-resize');
const minify = require('gulp-minify');
const changed = require('gulp-changed');
const newer = require('gulp-newer');
const livereload = require('gulp-livereload');
const child_process = require('child_process');
// paths
const BUILD_PATH = './public';
const DEV_PATH = './dev';
const SRC_PATH='./src';
const PATHS = {
  "sass":"./src/sass",
  "css":"./public/css",
}
//

// clean, copy, runserver
gulp.task('clean:dev', ()=>{
  return del(DEV_PATH)
})
gulp.task('clean:build',()=>{
  return del(BUILD_PATH)
})
//
var compileCSS = ()=> {
	return gulp.src([ `${PATHS.sass}/main.scss`])
        .pipe(sourcemaps.init())
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write({includeContent: false}))
        .pipe(sourcemaps.init({loadMaps:true}))
        .pipe(prefix('last 2 versions'))
        .pipe(sourcemaps.write('.'))
		.pipe(concat('master.css'))
        .pipe(minify())
		.pipe(gulp.dest(PATHS.css))
        .pipe(livereload())
        .pipe(notify({
			"title": "SASS styles compiled.",
			"message": "Compiled <%= file.relative %>",
		}));
  }


// TODO: configure compileHTML function to watch for changes to both english and spanish twig files


// comment out when working with just the spanish version of the twig files 
var compileHTML = ()=> {
  let out =  gulp.src([`${SRC_PATH}/index.twig`, `${SRC_PATH}/about.twig`])
  .pipe(plumber({
    handleError: function (err) {
       console.log(err);
       this.emit('end');
    }
  }))
  .pipe(twig())
  .pipe(replace("png", "jpg"))
  .pipe(gulp.dest(BUILD_PATH))
      .pipe(livereload());
    return out;
}


// comment out when working with just the english version of the twig files
/*var compileHTML = ()=>{
  
  var languages = ['es']
  return new Promise(function(resolve, reject){
    for (var i=0; i < languages.length; i++) {
      gulp.src('./src/translations/' + languages[i] +'/index.twig')
      .pipe(twig({
        data: {
          lang: languages[i]
        }
        })
      )
      .pipe(replace("png", "jpg"))
      .pipe(gulp.dest('./'+languages[i] + '/'))
    };
    gulp.src(`${SRC_PATH}/index.twig`)
    .pipe(twig())
    .pipe(replace("png", "jpg"))
    .pipe(gulp.dest('./'))
    gulp.src(`${SRC_PATH}/translations/es/index.twig`)
    .pipe(twig())
    .pipe(replace("png", "jpg"))
    .pipe(gulp.dest(`${BUILD_PATH}/es/`))
    resolve();
  })
  
}*/


var images = (done)=> {
  gulp.src(SRC_PATH + "/img/*.!(png)")
      .pipe(gulp.dest(BUILD_PATH + "/img"))
    // gulp.src(SRC_PATH + "/img/*.png")
    //     .pipe(imageResize({format:'jpeg'}))
    //     .pipe(rename({extname: ".jpg"}))
    //     .pipe(gulp.dest(BUILD_PATH + "/img"))
    // TODO: Above needs to be uncommented for the first complete build
  gulp.src(SRC_PATH + '/data/*.*')
      .pipe(gulp.dest(BUILD_PATH + "/data"))
  done();
}
function vendorJS(done) {
    return gulp.src(['node_modules/jquery/dist/jquery.min.js','node_modules/svg.js/dist/svg.min.js','node_modules/svg.filter.js/dist/svg.filter.min.js',"node_modules/bootstrap/dist/js/bootstrap.min.js",'node_modules/popper.js/dist/umd/popper.min.js','node_modules/waypoints/lib/jquery.waypoints.min.js', 'node_modules/waypoints/lib/shortcuts/inview.js', 'node_modules/lottie-web/build/player/lottie.min.js', 'node_modules/velocity-animate/velocity.min.js', 'node_modules/jquery.scrollto/jquery.scrollTo.min.js'])
        .pipe(concat('vendor.js'))
        .pipe(minify())
        .pipe(gulp.dest(`${BUILD_PATH}/js`))
        .pipe(livereload())
        done();
}
function appJS(done){
   return gulp.src(['./src/js/*.js'])
    .pipe(babel({
        presets: ['@babel/env' , '@babel/preset-react']
    }))
    .pipe(gulp.dest(`${BUILD_PATH}/js`))
       .pipe(livereload());
    done();

}

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch('./**/*.scss', compileCSS);
    gulp.watch('./**/*.twig', gulp.series(compileHTML));
    gulp.watch('./src/**/*.js', gulp.series(appJS));
    gulp.watch('./src/img/*.*', gulp.series(images))
})
gulp.task('js', gulp.series(vendorJS, appJS));
gulp.task('connect', function(done){
    connect.server({
        root:'public',
        host:'0.0.0.0',
        port:3000,
        middleware:function(connect){
            return[
                require('connect-livereload')()
            ]
        }
    })
    done();
})
// gulp.task('test:csscritic', gulp.series(function(){
//     return gulp.src('public/**/*.*')
//         .pipe(gulp.dest('testing'))
//     },
//     function(){
//         return gulp.src('test/regressionRunner.html')
//             .pipe(gulp.dest('testing'))
//     },
//     function(){
//     return gulp.src('node_modules/csscritic/dist/csscritic.allinone.js')
//         .pipe(rename("csscritic.js"))
//         .pipe(gulp.dest('testing/js'))
//     },
//     function(){
//        return connect.server({
//             root:'testing',
//             host:'0.0.0.0',
//             port:4000,
//         })

//     }
//     ))
gulp.task('e2e',()=>{
   return gulp.src('wdio.conf.js')
        .pipe(webdriver({
            loglevel:'verbose',
            waitforTimeout:10000
        }))
});

gulp.task('test:e2e', gulp.series('connect','e2e', (done)=>{ connect.serverClose(); done();}));
gulp.task('build', gulp.series(gulp.parallel(images, 'js', compileCSS, compileHTML))); // NOTE: Add images task back when fixed or manually transfer images to the public folder
gulp.task('deploy',gulp.series('build', (done)=>{child_process.exec('firebase deploy');done();}))
gulp.task('default', gulp.series('build', gulp.parallel('watch', 'connect')));

gulp.task('images', images);