
// document.getElementById('hero').scrollTo(1,100);
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);
// check for touch device
let isTouchDevice = ('ontouchstart' in window || 'onmsgesturechange' in window);
let iOS = navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

$(function(){
    /* window.scrollTo(0, 1);
    setTimeout(function () {
        window.scrollTo(0, 1);
    }, 1000) */

    // window.scrollTo(0,1);
    "use strict";
    // no scroll
    //$('body').css('overflow','hidden');
    // // no scroll mobile
    document.ontouchmove = (e)=>{
        e.preventDefault();
    }

    if(iOS) {
        $('html').addClass('iOS-device');
    } else {
        $('html').addClass('not-iOS-device');
    }

    let url = document.location.origin;
    console.log(url);

    $('nav a.nav-link--anchor').each(function(i, e){
        //console.log($(this.hash));
    })


    $('nav a.nav-link--anchor').click(function(e){
        let $target = $(this.hash);
        console.log($target);
        let targetOffset = $target.offset().top - 30;
        $('html,body').animate({scrollTop: targetOffset}, 'slow');
        $('#navbarToggleExternalContent').collapse('hide');
    })
    

    var words = ["dating?", "sex?", "questions?", "life?"];
    var colors = ["#FF3B7D", "#9013FE", "#2D8EFF", "#1EA03C"];
    var count = 1;
    setInterval(function(){          
        $("span.word").html(words[count]);
        $("span.word, .color-change").css('color', colors[count]);
        count++;        
        if(count == words.length)            
            count = 0;        
    }, 1500);

    // If we want to use the fade again
    /* setInterval(function(){    
        $("span.word").fadeIn(0, function(){        
            $(ths).html(words[count]);
            count++;        
            if(count == words.length)            
                count = 0;        
            $(this).fadeIn(0);    
        });
    }, 1500); */


    let homeImages;
    let aboutImages;
    let homeRandNum;
    let aboutRandNum;
    let id;

    function doneResizing(){
        if($(window).width() >= 992) {
            homeImages = ['bg-squad-grid-D-1', 'bg-squad-grid-D-2', 'bg-squad-grid-D-3', 'bg-squad-grid-D-4', 'bg-squad-grid-D-5', 'bg-squad-grid-D-6', 'bg-squad-grid-D-7'];
            aboutImages = ['bg-squad-grid-D-ABOUT-1', 'bg-squad-grid-D-ABOUT-2', 'bg-squad-grid-D-ABOUT-3', 'bg-squad-grid-D-ABOUT-4', 'bg-squad-grid-D-ABOUT-5', 'bg-squad-grid-D-ABOUT-6', 'bg-squad-grid-D-ABOUT-7'];
        } else if ($(window).width() < 992){
            homeImages = ['bg-squad-grid-1', 'bg-squad-grid-2', 'bg-squad-grid-3', 'bg-squad-grid-4', 'bg-squad-grid-5', 'bg-squad-grid-6', 'bg-squad-grid-7'];
            aboutImages = ['bg-squad-grid-ABOUT-1', 'bg-squad-grid-ABOUT-2', 'bg-squad-grid-ABOUT-3', 'bg-squad-grid-ABOUT-4', 'bg-squad-grid-ABOUT-5', 'bg-squad-grid-ABOUT-6', 'bg-squad-grid-ABOUT-7'];
        }
        
        homeRandNum = Math.floor(Math.random() * homeImages.length);
        aboutRandNum = Math.floor(Math.random() * aboutImages.length);

        let homeSrc = `img/${homeImages[homeRandNum]}.jpg`;
        let homeSrcSet = `img/${homeImages[homeRandNum]}@2x.jpg 2x`;

        let aboutSrc = `img/${aboutImages[aboutRandNum]}.jpg`;
        let aboutSrcSet = `img/${aboutImages[aboutRandNum]}@2x.jpg 2x`;

        $('.Home-page .Hero-grid').attr('src', homeSrc);
        $('.Home-page .Hero-grid').attr('srcset', homeSrcSet);

        $('.About-page .Hero-grid').attr('src', aboutSrc);
        $('.About-page .Hero-grid').attr('srcset', aboutSrcSet);
    }

    $(document).ready(function() {
        doneResizing();
    });

    $(window).resize(function() {
        clearTimeout(id);
        id = setTimeout(doneResizing, 200);
    });

    // nav positioning
    let checkWidth = function checkWidth(windowWidth) {
        isTouchDevice = ('ontouchstart' in window || 'onmsgesturechange' in window);
        if(($('section.hero').width() / $('section.hero').height()) > 1.7777778) {
            $('body').removeClass('portrait');
        } else {
            $('body').addClass('portrait');
        }

        if(isTouchDevice) {
            $('html').addClass('touch');
        } else {
            $('html').removeClass('touch');
        }

        if (windowWidth >= 992) {
            $('body').addClass('lg').removeClass('sm');
            console.log('large');
           
            
        } else if (windowWidth < 992) {
            $('body').addClass('sm').removeClass('lg');
            console.log('small');
            
        }
       
    };

    $(window).resize(()=> {
        checkWidth($(this).width());
    });
    // set initial position
    checkWidth($(window).width());
    
    

    // scroll to top on page reload
    /* window.onbeforeunload = function () {x
        window.scrollTo(0, 0);
    } */

    // display nav
    $('#nav-holder').removeClass('d-none');

    // bold the active page in the menu (english or spanish page)
    $(".lang-switch").each(function( index ) {
        if($(this).attr("href") == window.location.pathname) {
            $(this).addClass('active');
        }
    })

    //initialize animation
    let pth;
    var element = document.getElementById('lottie-container');
    let anim = lottie.loadAnimation({
       container: element, // the dom element that will contain the animation
       renderer: 'svg',
       loop: false,
       autoplay: true,
       path: pth, // the path to the animation json
        rendererSettings: {
           preserveAspectRatio:'xMidYMid slice'
        }
    });

     function animateLogo(e) {
        if (e.currentTime > (anim.totalFrames - 100)) {
            // console.log('stopped');
            $(element).css('z-index', '1');
            $('body').css('overflow', 'scroll');
            document.ontouchmove = (e) => {
                return true;
            }

            $('svg', element).velocity({
                scale: '80%',
            }, 300);
            anim.removeEventListener("enterFrame", animateLogo);

        }
    };

    anim.addEventListener("enterFrame",animateLogo); 


    $('.Connect--collapse',).on('show.bs.collapse', function(e){
        $(this).siblings('.Connect--block-wrapper').find('.Connect--button-top').addClass('hide');
        $(this).siblings('.Connect--block-wrapper').find('.Connect--button-top').removeClass('show');
    })
    $('.Connect--collapse',).on('hidden.bs.collapse', function(e){
        $(this).siblings('.Connect--block-wrapper').find('.Connect--button-top').addClass('show');
        $(this).siblings('.Connect--block-wrapper').find('.Connect--button-top').removeClass('hide');
    })

    // menu icon drawing code
    let menu_icon_code = `<g id="menu_icon">
            <rect id="rectangle-1" fill="#50E3C2" x="0" y="0" width="7" height="7"></rect>
            <rect id="rectangle-2" fill="#FF9F00" x="0" y="14" width="7" height="7"></rect>
            <rect id="rectangle-3" fill="#FF001F" x="0" y="28" width="7" height="7"></rect>
            <rect id="rectangle-4" fill="#9013FE" x="14" y="0" width="7" height="7"></rect>
            <rect id="rectangle-5" fill="#FFFFFF" x="14" y="14" width="7" height="7"></rect>
            <rect id="rectangle-6" fill="#7ED321" x="14" y="28" width="7" height="7"></rect>
            <rect id="rectangle-7" fill="#F8E71C" x="28" y="0" width="7" height="7"></rect>
            <rect id="rectangle-8" fill="#5FC7FF" x="28" y="14" width="7" height="7"></rect>
            <rect id="rectangle-9" fill="#FF02DB" x="28" y="28" width="7" height="7"></rect>
        </g>`;

    let menu_icon = SVG('menu-logo');
    menu_icon.svg(menu_icon_code);
    menu_icon.viewbox(0,0,42,42)
    menu_icon.select('#menu_icon').move(3,3)
    $('#navbarToggleExternalContent').on('show.bs.collapse', function(e){
            $('body.sm').css('overflow','hidden');
            document.ontouchmove = (e)=>{
                e.preventDefault();
            }
        let icon = SVG.get('menu-logo');
        //if on english page
        $('.menu-text' ).hide().text('CLOSE').fadeIn()
        
        animIn(icon);
        $('div.modal').fadeTo(200, 0.5).show().click(()=>{
            $('#navbarToggleExternalContent').collapse('hide');
        });

    })
    $('#navbarToggleExternalContent').on('hide.bs.collapse', function(e){
        $('body').css('overflow','scroll');
        document.ontouchmove = e=>{
            return true;
        }
        let icon = SVG.get('menu-logo');
        //if on english page
        $('.menu-text' ).hide().text('MENU').fadeIn()
        
        animOut(icon);
        $('div.modal').fadeTo(200, 0.0).hide().click(()=>{
            $('#navbarToggleExternalContent').collapse('show');
        });

    })
    let animIn = (icon)=>{
        let duration = 200;
        let rect1 = icon.select('#rectangle-1').animate(duration,'>',0).attr({x:-3, y:-3})
        let rect3 = icon.select('#rectangle-3').animate(duration,'>',0).attr({x:-3,y:31})
        let rect7= icon.select('#rectangle-7').animate(duration,'>',0).attr({x:31,y:-3})
        let rect9 = icon.select('#rectangle-9').animate(duration,'>',0).attr({x:31,y:31})
        let rect2 =icon.select('#rectangle-2').animate(duration,'>',0).attr({opacity:0})
        let rect4 =icon.select('#rectangle-4').animate(duration,'>',0).attr({opacity:0})
        let rect6 =icon.select('#rectangle-6').animate(duration,'>',0).attr({opacity:0})
        let rect8 =icon.select('#rectangle-8').animate(duration,'>',0).attr({opacity:0})
    }
    let animOut = (icon)=>{
        let duration = 200;
        let rect1 = icon.select('#rectangle-1').animate(duration,'>',0).attr({x:0, y:0})
        let rect3 = icon.select('#rectangle-3').animate(duration,'>',0).attr({x:0,y:28})
        let rect7= icon.select('#rectangle-7').animate(duration,'>',0).attr({x:28,y:0})
        let rect9 = icon.select('#rectangle-9').animate(duration,'>',0).attr({x:28,y:28})
        let rect2 =icon.select('#rectangle-2').animate(duration,'>',0).attr({x:0,opacity:1})
        let rect4 =icon.select('#rectangle-4').animate(duration,'>',0).attr({y:0,opacity:1})
        let rect6 =icon.select('#rectangle-6').animate(duration,'>',0).attr({y:28,opacity:1})
        let rect8 =icon.select('#rectangle-8').animate(duration,'>',0).attr({x:28,opacity:1})
    }
    let hoverIn = ()=>{
        let icon = SVG.get('menu-logo');
        let duration = 200;
        let rect1 = icon.select('#rectangle-1').animate(duration,'>',0).attr({x:-3, y:-3})
        let rect3 = icon.select('#rectangle-3').animate(duration,'>',0).attr({x:-3,y:31})
        let rect7= icon.select('#rectangle-7').animate(duration,'>',0).attr({x:31,y:-3})
        let rect9 = icon.select('#rectangle-9').animate(duration,'>',0).attr({x:31,y:31})
        let rect2 =icon.select('#rectangle-2').animate(duration,'>',0).attr({x:-3})
        let rect4 =icon.select('#rectangle-4').animate(duration,'>',0).attr({y:-3})
        let rect6 =icon.select('#rectangle-6').animate(duration,'>',0).attr({y:31})
        let rect8 =icon.select('#rectangle-8').animate(duration,'>',0).attr({x:31})

    }
    let hoverOut = ()=>{
        let icon = SVG.get('menu-logo');
        let duration = 200;
        let rect1 = icon.select('#rectangle-1').animate(duration,'>',0).attr({x:0, y:0})
        let rect3 = icon.select('#rectangle-3').animate(duration,'>',0).attr({x:0,y:28})
        let rect7= icon.select('#rectangle-7').animate(duration,'>',0).attr({x:28,y:0})
        let rect9 = icon.select('#rectangle-9').animate(duration,'>',0).attr({x:28,y:28})
        let rect2 =icon.select('#rectangle-2').animate(duration,'>',0).attr({x:0})
        let rect4 =icon.select('#rectangle-4').animate(duration,'>',0).attr({y:0})
        let rect6 =icon.select('#rectangle-6').animate(duration,'>',0).attr({y:28})
        let rect8 =icon.select('#rectangle-8').animate(duration,'>',0).attr({x:28})
    }
    $('.navbar-toggler').hover(()=>{
        hoverIn();
    }, ()=>{
        hoverOut();
    })
})
